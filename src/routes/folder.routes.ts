import { Router } from "express"
import { folderController } from "@controllers/folder.controller"
import { httpHandler } from "@middlewares/handler.middleware"
import { createFolderSchema } from "@middlewares/validators/folder/createFolder.schema"
import { detailsFolderRequestSchema } from "@middlewares/validators/folder/listFolderItem.schema"
import { renameFolderSchema } from "@middlewares/validators/folder/renameFolder"
import { uploadMiddleware } from "@middlewares/upload.middleware"
import { fileController } from "@controllers/file.contoller"
import { fileUploadSchema } from "@middlewares/validators/folder/fileUpload.schema"
import { deleteFolderSchema } from "@middlewares/validators/folder/deleteFolder.schema"

const folderRoutes = Router()

folderRoutes.post('/:fatherFolderId', httpHandler(folderController.create, createFolderSchema))
folderRoutes.get('/', httpHandler(folderController.details, detailsFolderRequestSchema))
folderRoutes.patch('/:id/name', httpHandler(folderController.rename, renameFolderSchema))
folderRoutes.post('/:folderId/file', uploadMiddleware, httpHandler(fileController.createFile, fileUploadSchema))
folderRoutes.delete('/:folderId', httpHandler(folderController.delete.bind(folderController), deleteFolderSchema))

export { folderRoutes }