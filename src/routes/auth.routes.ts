import { Router } from "express";
import { AuthController } from "@controllers/auth.controller";
import { httpHandler } from "@middlewares/handler.middleware";
import { loginSchema } from "@middlewares/validators/auth/login.schema";
import { signinSchema } from "@middlewares/validators/auth/signin.schema";

const authRoutes = Router()

authRoutes.post('/signin', httpHandler(AuthController.signin, signinSchema))
authRoutes.post('/login', httpHandler(AuthController.login, loginSchema))

export { authRoutes }