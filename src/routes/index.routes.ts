import { Router } from "express";
import { authMiddleware } from "@middlewares/auth.middleware";
import { authRoutes } from "./auth.routes";
import { folderRoutes } from "./folder.routes";
import { fileRoutes } from "./file.routes";

const routes = Router()

routes.use('/auth', authRoutes)
routes.use('/folder', authMiddleware, folderRoutes)
routes.use('/file', authMiddleware, fileRoutes)

export { routes }