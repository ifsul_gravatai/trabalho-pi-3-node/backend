import { HttpCode } from "@core/enums/httpCode";
import { HttpResponse } from "@core/interfaces/httpMethod";
import { AuthMessages } from "@core/messages/authMessages";
import { folderRepository } from "@repositories/folder/folderRepository";
import { userRepository } from "@repositories/user/userRepository";
import { LoginDTO } from "@shared/interfaces/dto/auth/loginDTO";
import { SigninDTO } from "@shared/interfaces/dto/auth/signinDTO";
import { AuthUtil } from "@util/util";
import { db } from "src/data-source";

class AuthController {

    public static async signin(input: SigninDTO): Promise<HttpResponse> {
        const { email } = input
        const user = await userRepository().findByEmail(email)

        if (user) {
            return { statusCode: HttpCode.INTERNAL_ERROR, json: { message: AuthMessages.USER_ALREADY_CREATED } }
        }

        try {
            await db.manager.transaction(async (entityManager) => {
                const { identifiers } = await userRepository(entityManager).create(input)

                const { id } = identifiers[0]

                await folderRepository(entityManager).createRootFolder(id)
            })

            return { statusCode: HttpCode.CREATED }
        } catch (err) {
            console.log(err);

            return { statusCode: HttpCode.INTERNAL_ERROR }
        }
    }

    public static async login(input: LoginDTO): Promise<HttpResponse> {
        const user = await userRepository().findUserLogin(input)

        if (!user) {
            return { statusCode: HttpCode.UNAUTHORIZED, json: { message: AuthMessages.USER_NOT_FOUND } }
        }

        try {
            const token = AuthUtil.createToken({ id: user.id })

            return { statusCode: HttpCode.OK, json: { token } }
        } catch (error) {
            console.log(error);

            return { statusCode: HttpCode.INTERNAL_ERROR, json: { message: AuthMessages.AUTH_FAILED } }
        }
    }
}

export { AuthController }