import { HttpCode } from "@core/enums/httpCode";
import { HttpResponse } from "@core/interfaces/httpMethod";
import { FolderMessages } from "@core/messages/folderMessages";
import { folderRepository } from "@repositories/folder/folderRepository";
import { FileUploadDTO } from "@shared/interfaces/dto/folder/fileUploadDTO";
import { join } from 'path'
import { unlink } from 'fs'
import { fileRepository } from "@repositories/file/fileRepository";
import { FileItem } from "@shared/interfaces/dto/folder/fileItem";
import { getFileType } from "@util/fileExtention";
import { DeleteFileDTO } from "@shared/interfaces/dto/file/deleteFileDTO";
import { FileMessages } from "@core/messages/fileMessages";

class FileController {

    public async createFile(input: FileUploadDTO): Promise<HttpResponse> {
        const { folderId, userId, filename, originalFilename } = input

        const folder = await folderRepository().findByIdAndUserId(folderId, userId)

        const filePath = join(__dirname, '../', 'uploads', filename)

        if (!folder) {
            unlink(filePath, () => { })

            return { statusCode: HttpCode.INTERNAL_ERROR, json: { message: FolderMessages.NOT_FOUND } }
        }


        try {
            const insert = await fileRepository().create(input)

            const id = insert.identifiers[0].id

            const file = await fileRepository().findById(id)

            const fileItem: FileItem = {
                id,
                name: file.originalFilename,
                type: getFileType(file.originalFilename)
            }

            return { statusCode: HttpCode.CREATED, json: { file: fileItem } }
        } catch (error) {
            unlink(filePath, () => { })

            return { statusCode: HttpCode.INTERNAL_ERROR }
        }

    }

    public async delete(input: DeleteFileDTO): Promise<HttpResponse> {
        const { id, userId } = input

        const file = await fileRepository().findByIdAndUser(id, userId)

        if (!file) {
            return { statusCode: HttpCode.NOT_FOUND, json: { message: FileMessages.FILE_NOT_FOUND } }
        }

        const { filename } = file

        const filePath = join(__dirname, '../', 'uploads', filename)

        try {
            unlink(filePath, () => { })

            await fileRepository().deleteById(id)

            return { statusCode: HttpCode.NO_CONTENT }
        } catch (error) {
            return { statusCode: HttpCode.INTERNAL_ERROR, json: { message: FileMessages.ERROR_ON_DELETE } }
        }
    }
}

export const fileController = new FileController()