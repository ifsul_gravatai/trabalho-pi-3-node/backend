export enum FolderMessages {
    NOT_FOUND_FATHER = 'Diretório pai não encontrado',
    NOT_FOUND = 'Diretório não encontrado',
    FOLDER_ALREADY_EXIST = 'Pasta já existe neste diretório'
}