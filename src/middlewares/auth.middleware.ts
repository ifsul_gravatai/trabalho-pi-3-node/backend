import { NextFunction, Request, Response } from "express"
import { HttpCode } from "@core/enums/httpCode"
import { AuthMessages } from "@core/messages/authMessages"
import { AuthUtil } from "@util/util"

export const authMiddleware = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { authorization } = req.headers

    if (!authorization) {
        res.status(HttpCode.UNAUTHORIZED).json(AuthMessages.UNAUTHORIZED)

        return
    }

    const token = authorization.replace('Bearer ', '')

    const jwt = AuthUtil.verifyJwt(token)

    if (!jwt) {
        res.status(HttpCode.UNAUTHORIZED).json(AuthMessages.UNAUTHORIZED)

        return
    }

    req.body.userId = jwt.id

    next()
}