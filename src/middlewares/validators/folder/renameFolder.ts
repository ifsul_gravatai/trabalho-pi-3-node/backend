import { RenameFolderDTO } from '@shared/interfaces/dto/folder/renameFolderDTO'
import joi from 'joi'

export const renameFolderSchema = joi.object<RenameFolderDTO>({
    id: joi.number().required(),
    name: joi.string().regex(/[/]/, { invert: true }).required()
})