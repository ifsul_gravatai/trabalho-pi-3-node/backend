import { CreateFolderDTO } from '@shared/interfaces/dto/folder/createFolderDTO'
import joi from 'joi'

export const createFolderSchema = joi.object<CreateFolderDTO>({
    fatherFolderId: joi.number().required(),
    folderName: joi.string().regex(/[/]/, { invert: true }).required()
})