import { LoginDTO } from '@shared/interfaces/dto/auth/loginDTO'
import joi from 'joi'

export const loginSchema = joi.object<LoginDTO>({
    email: joi.string().email().required(),
    password: joi.string().min(6).required(),
})