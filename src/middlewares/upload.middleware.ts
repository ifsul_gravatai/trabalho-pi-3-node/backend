import { NextFunction, Request, Response } from "express";
import { UploadHandler } from "src/handler/upload.handler";
import { pipelineAsync } from '@util/util'
import { FileUploadHandlerDTO } from "@shared/interfaces/dto/folder/fileUploadHandlerDTO";

export const uploadMiddleware = async (request: Request, response: Response, next: NextFunction): Promise<void> => {
    const { headers, query, body } = request

    const input: FileUploadHandlerDTO = {
        ...body,
        ...query,
    }

    const uploadHandler = new UploadHandler(input.socketId)

    const onFinish = (filename: string, originalFilename: string, size: number) => {
        request.body = {
            ...request.body,
            filename,
            originalFilename,
            size
        }

        return next;
    }

    const busboyInstance = uploadHandler.registerEvents(headers, onFinish.bind(this))

    await pipelineAsync(request, busboyInstance)
}