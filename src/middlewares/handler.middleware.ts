import { HttpCode } from "@core/enums/httpCode";
import { HttpMethod } from "@core/interfaces/httpMethod";
import { buildErrorMessages } from "@util/errorMessage";
import { schemaValidator } from "@util/schemaValidator";
import { Handler, Request, Response } from "express";
import { ObjectSchema } from "joi";

export const httpHandler = <T = any>(method: HttpMethod<T>, validator?: ObjectSchema<T>): Handler => {
    return async (req: Request, res: Response): Promise<void> => {
        let input = {
            ...req.body,
            ...req.params,
            ...req.query
        }

        if (validator) {
            const { error, value } = schemaValidator(input, validator)

            if (error) {
                res.status(HttpCode.BAD_REQUEST).json({ message: buildErrorMessages(error) })
                return
            }

            input = value
        }

        input.userId = req.body.userId

        const { statusCode, json } = await method(input)

        const errorResponse = statusCode >= 400 ? { message: 'Unknow error' } : undefined

        res.status(statusCode).json(json || errorResponse)
    }
}