import Express from 'express'
import cors from 'cors'
import { createServer } from 'http'
import { Server } from 'socket.io'
import { routes } from '../routes/index.routes'

const app = Express()
const bootstrap = createServer(app)
const io = new Server(bootstrap, {
    cors: {
        origin: '*',
        credentials: false
    }
})


app.use(Express.json())
app.use(cors())

app.use(routes)

// io.on('connection', (socket) => {
//     console.log(`User ${socket.id} has connected!`);

//     socket.on('disconnect', () => {
//         console.log(`User ${socket.id} was disconnect!`);
//     })
// })

const interval = setInterval(() => {
    io.emit('call', 'Chamando todos os Autobots')
}, 2000)

export { bootstrap, io }