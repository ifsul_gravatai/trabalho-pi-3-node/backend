import { User } from "@entity/User";
import { encryptMd5 } from "@util/encrypt";
import { db } from "src/data-source";
import { EntityManager, Repository } from "typeorm";
import { UserCreate } from "./dto/userCreate";
import { UserLogin } from "./dto/userLogin";

class UserRepository {
    private reposiroty: Repository<User>

    constructor(entityManager?: EntityManager) {
        this.reposiroty = entityManager ? entityManager.getRepository(User) : db.manager.getRepository(User)
    }

    findByEmail(email: string) {
        return this.reposiroty.findOne({ where: { email } })
    }

    create(user: UserCreate) {
        return this.reposiroty.insert({
            ...user,
            password: encryptMd5(user.password)
        })
    }

    findUserLogin(user: UserLogin) {
        const { email, password } = user

        return this.reposiroty.findOne({
            where: {
                email,
                password: encryptMd5(password)
            }
        })
    }
}

export const userRepository = (entityManager?: EntityManager) => {
    return new UserRepository(entityManager)
}