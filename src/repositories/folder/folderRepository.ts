import { Folder } from "@entity/Folder"
import { db } from "src/data-source"
import { EntityManager, Repository, IsNull, Not, } from "typeorm"
import { CreateFolder } from "./dto/createFolder"

class FolderRepository {
    private instance: Repository<Folder>

    constructor(entityManager?: EntityManager) {
        this.instance = entityManager ? entityManager.getRepository(Folder) : db.manager.getRepository(Folder)
    }

    getInstance() {
        return this.instance;
    }

    createRootFolder(userId: number) {
        return this.create({ userId, name: null, path: null })
    }

    findById(id: number) {
        return this.instance.findOne({ where: { id } })
    }

    findByIdAndUserId(id: number, userId: number) {
        return this.instance.findOne({ where: { id, userId } })
    }

    findRootIdByUserId(userId: number) {
        return this.instance.findOne({ where: { userId, fatherFolderId: null } })
    }

    create(folder: CreateFolder) {
        return this.instance.insert(folder)
    }

    findByPath(path: string, userId: number) {
        let newPath = null

        if (path) {
            const [firstChar] = path.split('')
            newPath = firstChar === '/' ? path : `/${path}`
        }

        const pathPredicate = newPath || IsNull()

        return this.instance.findOne({
            where: { userId, path: pathPredicate }
        })
    }

    findChildrens(path: string, userId: number) {
        let newPath = null

        if (path) {
            const [firstChar] = path.split('')
            newPath = firstChar === '/' ? path : `/${path}`
        }

        const pathPredicate = newPath || IsNull()

        return this.instance.find({
            where: {
                userId,
                path: Not(pathPredicate),
                fatherFolder: {
                    path: pathPredicate
                }
            },
            order: {
                name: 'ASC'
            }
        })
    }

    delete(id: number) {
        return this.instance.delete(id)
    }
}

export const folderRepository = (entityManager?: EntityManager) => {
    return new FolderRepository(entityManager)
}