export interface CreateFile {
    filename: string,
    originalFilename: string,
    userId: number,
    folderId: number
}