import { File } from "@entity/File"
import { db } from "src/data-source"
import { EntityManager, Repository } from "typeorm"
import { CreateFile } from "./dto/createFile"

class FileRepository {
    private instance: Repository<File>

    constructor(entityManager?: EntityManager) {
        this.instance = entityManager ? entityManager.getRepository(File) : db.manager.getRepository(File)
    }

    getInstance() {
        return this.instance;
    }

    create(file: CreateFile) {
        return this.instance.insert(file)
    }

    findById(id: number) {
        return this.instance.findOne({
            where: {
                id
            }
        })
    }

    findByIdAndUser(id: number, userId: number) {
        return this.instance.findOne({
            where: {
                id,
                userId
            }
        })
    }

    findByFolderId(folderId: number) {
        return this.instance.find({
            where: {
                folderId
            },
            order: {
                originalFilename: 'ASC'
            }
        })
    }

    deleteByFolderId(folderId: number) {
        return this.instance.delete({ folderId })
    }

    deleteById(id: number) {
        return this.instance.delete(id)
    }
}

export const fileRepository = (entityManager?: EntityManager) => {
    return new FileRepository(entityManager)
}