import "reflect-metadata"
import { DataSource } from "typeorm"
import { Folder } from "./entity/Folder"
import { User } from "./entity/User"

export const db = new DataSource({
    type: "mysql",
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    synchronize: true,
    logging: false,
    entities: [`${__dirname}/entity/*.{js,ts}`],
    migrations: ['./migrations/*'],
    subscribers: []
})
