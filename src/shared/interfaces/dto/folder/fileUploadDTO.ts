import { Authenticated } from "../auth/AuthPayload";

export interface FileUploadDTO extends Authenticated {
    filename: string
    originalFilename: string
    size: number
    folderId: number
}