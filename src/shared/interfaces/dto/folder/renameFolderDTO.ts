import { Authenticated } from "../auth/AuthPayload"

export interface RenameFolderDTO extends Authenticated {
    id: number
    name: string
}