import { Authenticated } from "../auth/AuthPayload"

export interface DeleteFolderDTO extends Authenticated {
    folderId: number
}