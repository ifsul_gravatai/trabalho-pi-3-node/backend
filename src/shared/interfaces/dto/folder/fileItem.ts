import { FileType } from "@core/enums/fileType"

export interface FileItem {
    id: number,
    name: string
    type: FileType
}