import { Authenticated } from "../auth/AuthPayload";

export interface DeleteFileDTO extends Authenticated {
    id: number
}