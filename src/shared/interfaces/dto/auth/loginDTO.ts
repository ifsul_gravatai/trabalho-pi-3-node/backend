import { UserLogin } from "@repositories/user/dto/userLogin";

export interface LoginDTO extends UserLogin { }