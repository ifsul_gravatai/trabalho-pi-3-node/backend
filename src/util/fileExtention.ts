import { FileType } from "@core/enums/fileType";

const mapper: Record<string, FileType> = {
    jpeg: FileType.IMG,
    png: FileType.IMG,
    jpg: FileType.IMG,
    pdf: FileType.PDF,
    doc: FileType.DOC,
    mp4: FileType.VIDEO
}

export const getFileType = (filename: string): FileType => {
    const lastDot = filename.lastIndexOf('.')

    const extention = filename.slice(lastDot + 1)

    const type = mapper[extention]

    return type || FileType.UNKNOWN
}