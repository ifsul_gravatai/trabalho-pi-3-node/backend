import { ValidationError } from "joi"

export interface ErrorMessage {
    field: string | undefined
    key: string
}

export const buildErrorMessages = (error: ValidationError): ErrorMessage[] => {
    return error.details.map((value) => {
        const { context, message } = value

        return {
            field: context?.label,
            key: message
        }
    })
}