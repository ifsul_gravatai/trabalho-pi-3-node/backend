import { ObjectSchema, ValidationError, ValidationOptions } from "joi";

export interface ValidatorResul<T = {}> {
    error: ValidationError,
    value: T
}

export type SchemaValidator<T = {}> = (object: T, schema: ObjectSchema<T>, options?: ValidationOptions) => ValidatorResul<T>

export const schemaValidator: SchemaValidator = (object, schema, options) => {
    return schema.validate(object, {
        abortEarly: false,
        messages,
        stripUnknown: true,
        ...options
    })
}

const messages = {
    'any.required': 'field.required',
    'string.email': 'invalid.email',
    'string.min': 'min.value',
    'cpf.invalid': 'invalid.cpf',
    'number.base': 'field.must.be.number',
    'string.pattern.invert.base': 'field.inverted.pattern.not.match',
    'object.unknown': 'field.not.allowed'
}