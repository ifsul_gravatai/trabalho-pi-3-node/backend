import jwt, { JwtPayload } from 'jsonwebtoken'
import { AuthMessages } from '../core/messages/authMessages'

const createToken = (body: string | object | Buffer): string => {
    if (!process.env.SECRET) throw new Error(AuthMessages.NO_SECRET_DEFINED)

    return jwt.sign(body, process.env.SECRET, { expiresIn: '30m' })
}

const verifyJwt = (token: string): JwtPayload | null => {
    try {
        if (!process.env.SECRET) throw new Error(AuthMessages.NO_SECRET_DEFINED)

        const tokenDecoded = jwt.verify(token, process.env.SECRET)

        if (typeof tokenDecoded === 'string' || tokenDecoded === null) return null

        return tokenDecoded as JwtPayload
    } catch {
        return null
    }
}

export const auth = { createToken, verifyJwt }