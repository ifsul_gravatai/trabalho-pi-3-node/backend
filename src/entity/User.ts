import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn } from "typeorm"
import { Audit } from "./Audit"
import { Folder } from "./Folder"

@Entity()
export class User extends Audit {

    @PrimaryGeneratedColumn()
    id: number

    @Column({ type: 'varchar' })
    name: string

    @Column({ type: 'varchar', unique: true })
    email: string

    @Column({ type: 'varchar' })
    password: string

    @OneToMany(() => Folder, folder => folder.user)
    @JoinColumn()
    folders: Promise<Folder[]>

}