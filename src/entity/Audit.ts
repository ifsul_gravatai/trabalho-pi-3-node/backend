import { CreateDateColumn, UpdateDateColumn } from "typeorm"

export abstract class Audit {

    @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)", nullable: false })
    createdAt: Date

    @UpdateDateColumn({ type: "timestamp", default: null, onUpdate: "CURRENT_TIMESTAMP(6)" })
    updatedAt: Date
}