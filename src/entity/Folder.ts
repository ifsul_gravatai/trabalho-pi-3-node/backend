import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, Unique, OneToMany, JoinColumn } from "typeorm"
import { Audit } from "./Audit"
import { User } from "./User"

@Entity()
@Unique('user_path_UNIQUE', ['path', 'userId'])
export class Folder extends Audit {

    @PrimaryGeneratedColumn()
    id: number

    @Column({ type: 'varchar', nullable: true, default: null })
    name: string

    @Column({ type: 'varchar', nullable: true, default: null })
    path: string

    @Column({ type: 'int', nullable: false })
    @JoinColumn()
    userId: number

    @Column({ type: 'int', nullable: true })
    @JoinColumn()
    fatherFolderId: number

    @ManyToOne(() => User, user => user.folders)
    @JoinColumn()
    user: Promise<User>

    @OneToMany(() => Folder, folder => folder.fatherFolder)
    @JoinColumn()
    folders: Promise<Folder[]>

    @ManyToOne(() => Folder, folder => folder.folders)
    @JoinColumn()
    fatherFolder: Promise<Folder>

}