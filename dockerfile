FROM nginx:1.18

RUN rm -rf /usr/share/nginx/html/*

COPY ./build/ /usr/share/nginx/html

EXPOSE 8010